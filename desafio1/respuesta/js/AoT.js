/* 
Luis Sandoval
*/
// QUERRY SELECTORS
const btn_avis = document.querySelector('#btn-avi');
// EVENTOS
btn_avis.addEventListener('click',mostrarDatos);
// let  = document.querySelector('#')
let tabla = document.querySelector('#datos');
//Meses
const Enero = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30];
const Febrero = [31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58];
const Marzo = [59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89];
const Abril = [90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119];
const Mayo = [120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149];
const Junio = [150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180];
const Julio = [181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211];
const Agosto = [212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241];
const Septiembre = [242,243,244,245,246,247,248,249,250,251,252,253,254,255,256,257,258,259,260,261,262,263,264,265,266,267,268,269,270,271,272];
const Octubre = [273,274,275,276,277,278,279,280,281,282,283,284,285,286,287,288,289,290,291,292,293,294,295,296,297,298,299,300,301,302,303];
const Noviembre = [304,305,306,307,308,309,310,311,312,313,314,315,316,317,318,319,320,321,322,323,324,325,326,327,328,329,330,331,332,333];
const Diciembre = [334,335,336,337,338,339,340,341,342,343,344,345,346,347,348,349,350,351,352,353,354,355,356,357,358,359,360,361,362,363,364];
const Meses = [Enero,Febrero,Marzo,Abril,Mayo,Junio,Julio,Agosto,Septiembre,Octubre,Noviembre,Diciembre]
// FUNCIONES
function mostrarDatos(){
    //console.log('mostrar datos');
    const xhttp = new XMLHttpRequest();
    xhttp.open('GET','./last_year.json',true);

    xhttp.send();
    xhttp.onreadystatechange = function(){
        if (this.readyState == 4 && this.status == 200) {
            //console.log(this.responseText);
            let datos = JSON.parse(this.responseText);
            //console.log(datos);
            let ultimo_dia = buscar_ultimo_dia(datos);
            let avistamientos_por_dia = new Map();
            let pocos_avistamientos = 9999999;
            let respuesta;
            let mes;
            //console.log('hola'+ultimo_dia);
            for (let i = 0; i < ultimo_dia; i++) {
                let cantidad = 0;
                for (let index of datos) {
                    if (index[0]<=i && i<=index[1] ) { //podemos decir que un titan fue visto ese dia
                        cantidad+=1; //cuento el avistamiento
                    }
                }
                avistamientos_por_dia.set(i,cantidad);
            };
            //console.log(avistamientos_por_dia);
            let i =0;
            for(let e of avistamientos_por_dia){
                //console.log('el dia es: '+e[0])
                if (mesvalido(e[0]) == true) {
                    //console.log('dia: '+e[0]+' valido:'+mesvalido(e[0]))
                    if(e[1] < pocos_avistamientos){
                        pocos_avistamientos=e[1];
                        respuesta = e;
                        mes = Quemes(e[0]+1);
                    }
                    
                }
            };
            tabla.innerHTML+=`
                <tr>
                    <td>${mes}</td>
                    <td>${respuesta[0]+1}</td>
                    <td>${respuesta[1]}</td>
                </tr>
            `
        };
    };
};

function buscar_ultimo_dia(d){
    let i = 0;
    d.forEach(e => {
        //console.log('aqui'+parseInt(e[1]));
        if (i < e[1] ) {i = e[1];}
    });
    return i;
};

function mesvalido(m){
    let mes;
    //console.log('revisando dia: '+m)
    if (m+1>364) {m-=364;};
    Meses.forEach(e => {
        e.forEach(a => {
            if (a == m+1) {mes = e;}
        });
    });
    //console.log(Enero);
    if (mes === Enero || mes === Diciembre) {
        return false;
    }else{
        return true;
    }
};

function Quemes(d){
    let a;
    if (esEnero(d)) {return a='Enero'};
    if (esFebrero(d)) {return a='Febrero'};
    if (esMarzo(d)) {return a='Marzo'};
    if (esAbril(d)) {return a='Abril'};
    if (esMayo(d)) {return a='Mayo'};
    if (esJunio(d)) {return a='Junio'};
    if (esJulio(d)) {return a='Julio'};
    if (esAgosto(d)) {return a='Agosto'};
    if (esSeptiembre(d)) {return a='Septiembre'};
    if (esOctubre(d)) {return a='Octubre'};
    if (esNoviembre(d)) {return a='Noviembre'};
    if (esDiciembre(d)) {return a='Diciembre'};
};
    
    

function esEnero(d){
    flag = false;
    if (d>364) {
        d-=364;
        //console.log('hola'+m);
    };
    Enero.forEach(e => {
        if (e==d) {
            flag = true;
        };
    });
    return flag;
};

function esFebrero(d){
    flag = false;
    if (d>364) {
        d-=364;
        //console.log('hola'+m);
    };
    Febrero.forEach(e => {
        if (e==d) {
            flag = true;
        };
    });
    return flag;
};

function esMarzo(d){
    flag = false;
    if (d>364) {
        d-=364;
        //console.log('hola'+m);
    };
    Marzo.forEach(e => {
        if (e==d) {
            flag = true;
        };
    });
    return flag;
};

function esAbril(d){
    flag = false;
    if (d>364) {
        d-=364;
        //console.log('hola'+m);
    };
    Abril.forEach(e => {
        if (e==d) {
            flag = true;
        };
    });
    return flag;
};

function esMayo(d){
    flag = false;
    if (d>364) {
        d-=364;
        //console.log('hola'+m);
    };
    Mayo.forEach(e => {
        if (e==d) {
            flag = true;
        };
    });
    return flag;
};

function esJunio(d){
    flag = false;
    if (d>364) {
        d-=364;
        //console.log('hola'+m);
    };
    Junio.forEach(e => {
        if (e==d) {
            flag = true;
        };
    });
    return flag;
};

function esJulio(d){
    flag = false;
    if (d>364) {
        d-=364;
        //console.log('hola'+m);
    };
    Julio.forEach(e => {
        if (e==d) {
            flag = true;
        };
    });
    return flag;
};

function esAgosto(d){
    flag = false;
    if (d>364) {
        d-=364;
        //console.log('hola'+m);
    };
    Agosto.forEach(e => {
        if (e==d) {
            flag = true;
        };
    });
    return flag;
};

function esSeptiembre(d){
    flag = false;
    if (d>364) {
        d-=364;
        //console.log('hola'+m);
    };
    Septiembre.forEach(e => {
        if (e==d) {
            flag = true;
        };
    });
    return flag;
};

function esOctubre(d){
    flag = false;
    if (d>364) {
        d-=364;
        //console.log('hola'+m);
    };
    Octubre.forEach(e => {
        if (e==d) {
            flag = true;
        };
    });
    return flag;
};

function esNoviembre(d){
    flag = false;
    if (d>364) {
        d-=364;
        //console.log('hola'+m);
    };
    Noviembre.forEach(e => {
        //console.log(e==d)
        if (e==d) {
            flag = true;
        }
    });
    return flag;
};

function esDiciembre(d){
    flag = false
    if (d>364) {
        d-=364;
        //console.log('hola'+m);
    };
    Diciembre.forEach(e => {
        if (e==d) {
            flag = true;
        };
    });
    return flag;
};