use test
DELIMITER $$
/*consulta 1*/
select t.nombre,t.altura,m.fecha from muertes as m
inner join titanes as t
on t.id = m.id_titan
where m.causa = 'Accidente'
order by m.fecha;
$$ 
/*consulta 2*/
select t.nombre, t.altura from titanes as t
inner join muertes as m
on t.id = m.id_titan
where m.causa = 'Batallón 1' and t.altura = (
select max(t.altura) from titanes as t
inner join muertes as m
on t.id = m.id_titan
where m.causa = 'batallón 1');
$$
/*consulta 3*/
/*
titanes vivos
select t.id from titanes as t
where not exists (select 1 from muertes as m where m.id_titan = t.id);

titanes muertos
select t.id from titanes as t
right join muertes as m
on t.id = m.id_titan;

select fecha from avistamientos where id_titan = 1 
*/
/*solucion consulta 3*/
select t.nombre, t.altura, av.fecha from titanes as t
inner join avistamientos as av
where not exists (select 1 from muertes as m where m.id_titan = t.id)
and av.fecha = (select max(fecha) from avistamientos where id_titan = t.id);
$$
/*consulta 4*/
drop function if exists multiavistamiento;
$$
create function multiavistamiento(ide int)
returns boolean
deterministic
begin
	declare suma int(11);
    declare primeravistamiento year;
    declare ultimoavistamiento year;
    set suma = 0;
    select extract(year from min(a.fecha)) into primeravistamiento from avistamientos as a where a.id_titan = ide;
    select extract(year from max(a.fecha)) into ultimoavistamiento from avistamientos as a where a.id_titan = ide;
    if primeravistamiento = ultimoavistamiento then
		select count(a.fecha) into suma from avistamientos as a where a.id_titan = ide;
		if suma > 2 then
			return true;
		end if;
    else
		while ultimoavistamiento > primeravistamiento do
			select count(a.fecha) into suma from avistamientos as a where a.id_titan = ide and year(a.fecha) = primeravistamiento;
			if suma > 2 then
				return true;
			else
				set primeravistamiento = primeravistamiento+1;
			end if;
		end while;
	end if;
	return false;
end;
$$
select t.nombre,t.altura from titanes as t
inner join  avistamientos as a
on t.id = a.id_titan
where multiavistamiento(t.id)
group by t.nombre
order by t.altura;
$$
/*consulta 5*/
drop function if exists killedTitansize;
$$
create function killedTitansize(id int)
returns int
deterministic
begin
	declare alto int;
    select t.altura into alto from muertes as m
    inner join titanes as t
    on m.id_titan = t.id
    where m.id = id;
    return alto;
end;
$$
select r.nombre as recurso, mr.cantidad, r.unidad as unidad_de_medida from recursos as r
inner join movimientos_recursos as mr
on r.id = mr.id_recurso
where 5 >= killedTitansize(mr.id_muerte);
$$
/*consulta 6*/
select r.nombre, count(mr.id_recurso) as utilizado from movimientos_recursos as mr
inner join recursos as r
on mr.id_recurso = r.id
where killedTitansize(mr.id_muerte) = 9
group by mr.id_recurso
having utilizado > 1
order by utilizado desc
limit 1;
$$
/*consulta 7*/
drop function if exists ultimoAvistamiento;
$$
create function ultimoAvistamiento(id int)
returns date
deterministic
begin
	declare ultimo date;
	select max(a.fecha) into ultimo from avistamientos as a where a.id_titan = id;
    return ultimo;
end;

$$
select t.nombre, m.fecha, ultimoAvistamiento(t.id) as ultimo_avistamiento  from muertes as m
inner join titanes as t
on m.id_titan = t.id
where m.fecha < ultimoAvistamiento(t.id)
order by m.fecha;

/* 8 teoria:
Estas incongruencias pueden ser causas de que un falso o mal reporte de muerte o debido a que tiene caracteristicas
similares a uno fallecido y se registro sin haber una comprobacion previa de que ese titan ya estaba fallecido lo que probocaria
la perdida del registro de un nuevo titan y el incremento de reportes erroneos de avistamientos de dicho titan.
*/