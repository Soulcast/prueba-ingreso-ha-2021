"use strict";
document.addEventListener('DOMContentLoaded', () => {
    // SELECTORES
    const recurso = document.querySelector('#recursos');
    const agregar = document.querySelector('.agregar');
    const main_conteiner = document.querySelector('.main-conteiner');
    // EVENTOS
    cargarEventListeners();
    function cargarEventListeners(){
        //cuando selecionas un recurso, se muestra el btn ingresar
        recurso.addEventListener('change',() =>{
            agregar.classList.remove('ocultar')
        });
        agregar.addEventListener('click', creartabla);
        agregar.addEventListener('click',tablaActiva);


    };
    // crear evento cuando la clase cambie para agregar mi tabla
    //console.log(agregar);
    
});
// FUNCIONES
const tb_array = [];

function recurso(e){
    //console.log('entro '+e);
    const input_cantidad = document.querySelector('.input-cantidad');
    const evaluar = e;
    switch(evaluar){
        case 'agua':
            input_cantidad.innerHTML =`
            <form class="form-cantidad">
            <input aria-label="cantidad" type="text" name="" onkeypress="return texto(event)" id="cantidad" required pattern="[0-9]+">
            <p class="unidad">L</p>
            </form>`
            break;
        case 'polvora':
            input_cantidad.innerHTML =`
            <form class="form-cantidad">
            <input aria-label="cantidad" type="text" name="" onkeypress="return texto(event)" id="cantidad" required pattern="[0-9]+">
            <p class="unidad">gr</p>
            </form>`
            break;
        case 'hoja_metal':
            input_cantidad.innerHTML =`
            <form class="form-cantidad">
            <input aria-label="cantidad" type="text" name="" onkeypress="return texto(event)" id="cantidad" required pattern="[0-9]+">
            <p class="unidad">u</p>
            </form>`
            break;
        case 'gas':
            input_cantidad.innerHTML =`
            <form class="form-cantidad">
            <input aria-label="cantidad" type="text" name="" onkeypress="return texto(event)" id="cantidad" required pattern="[0-9]+">
            <p class="unidad">L</p>
            </form>`
            break;
        case 'equipo_maniobra':
            input_cantidad.innerHTML =`
            <form class="form-cantidad">
            <input aria-label="cantidad" type="text" name="" onkeypress="return texto(event)" id="cantidad" required pattern="[0-9]+">
            <p class="unidad">u</p>
            </form>`
            break;
        default:
            break
    }
};
// al darle al btn ingresar iniciamos la funcion crear tabla
function creartabla(e){
    // previene acciones por defecto ej action,refresh
    e.preventDefault();
    // obtenemos el valor del text input
    const c = document.querySelector('#cantidad');
    const unidad = document.querySelector('.unidad');
    const recurso = document.querySelector('#recursos')
    const table = document.querySelector('.tabla-recursos');
    
    //por defecto es un string con el sig formato ej Tue Jul 13 2021 05:21:09 GMT-0400 (hora estándar de Chile)
    const d = Date();
    /* buscar error console.log muestra undefined
    const YY = d.getFullYear();
    const MM = d.getMonth();
    const DD = d.getDate();
    const hh = d.getHours();
    const mn = d.getHours();
    */
    const num = parseFloat(c.value);
    if( !isNaN(num) && num > 0){
        // es un valor valido
        const u = unidad.textContent;
        const cu = c.value.concat(' ',u);
        const nombre = recurso.value;
        const item = {nombre,cu,d};
    // crear un objeto con tipo recurso, cantidad+unidad,fecha asignarlo a un array para luego llenar la tabla
        tb_array.push(item);
        table.classList.remove('ocultar')
        //console.log(table)

    }else{
        alert('ingrese un numero mayor a 0');
    }

};

//tablaActiva();
function tablaActiva(){
    const tbody = document.querySelector('.table')
    const table = document.querySelector('.tabla-recursos');
    if (table.classList.contains('ocultar')) {
        
    }else{
        tbody.innerHTML = '';
        tb_array.forEach((e,i) => {
            console.log(i);
            tbody.innerHTML+=`
            <tr>
                <td>${e.nombre}</td>
                <td>${e.cu}</td>
                <td>${e.d}</td>
                <td><button type="submit" onclick=borrar(${i})><span style="color: Tomato"><i class="fas fa-trash-alt"></i></span></button></td>
            </tr>
            `;
        });
    };
};

function borrar(pos){
    const tbody = document.querySelector('.table')
    tbody.innerHTML = '';
    tb_array.splice(pos,1);
    tb_array.forEach((e,i) => {
        console.log(i);
        tbody.innerHTML+=`
        <tr>
            <td>${e.nombre}</td>
            <td>${e.cu}</td>
            <td>${e.d}</td>
            <td><button type="submit" onclick=borrar(${i})><span style="color: Tomato"><i class="fas fa-trash-alt"></i></span></button></td>
        </tr>
        `;
    });
};